import express from 'express';
import cookieSession from 'cookie-session';
import dotenv from 'dotenv';

dotenv.config();

import { routes } from './routes';

const app = express();

const cookieSecret = process.env.COOOKIE_SECRET;
app.use(cookieSession({ keys: [cookieSecret] }));
app.use(express.json());

app.use(routes);

app.use((err, req, res, next) => {
    res.status(400).send(`
        <h2>Oooops</h2>
        <p>${err.message || 'Что-то пошло не так'}</p>
    `)
});

const port = 3434;
app.listen(port, () => {
    console.log(`Server listening on http://localhost:${port}`);
});
