import { Router } from 'express';
import bkfd2Password from 'pbkdf2-password';
import _ from 'lodash';
import jwt from 'jsonwebtoken';
import expressJwt from 'express-jwt';

import { Role } from '../model/roles';

const router = Router();

const users: any[] = [];
const hash = bkfd2Password()

router.post('/user', (req, res) => {
    const { login, password } = req.body;

    hash({ password }, (err, pass, salt, hash) => {
        if (err) throw err;

        const newUser = { id: _.uniqueId(), login, hash, salt, role: Role.USER };
        
        users.push(newUser);
        res.send(newUser);
    });
});

router.post('/login', (req, res, next) => {
    const { login, password } = req.body;

    const user = users.find(u => u.login === login);

    if (user) {
        hash({ password, salt: user.salt }, (err, pass, salt, hash) => {
            if (err) throw err;

            if (hash === user.hash) {
                
                const { salt, hash, ...rest } = user;
                const token = jwt.sign({ id: user.id, role: user.role }, process.env.JWT_SECRET, { algorithm: 'HS256' })
                req.session.user = rest;

                res.send({ ...rest, token });
            } else {
                next(new Error('Пароль не подходит'));
            }
        });
    } else {
        throw new Error('Пользователь с таки login не найден.')
    }
});

router.get('/users', expressJwt({ secret: process.env.JWT_SECRET, algorithms: ['HS256'] }), (req: any, res, next) => {
    console.log(req.user)
    res.send(users);
});

export default router;
